const mongoose = require('mongoose');
const Product = require('../Models/productSchema');
const auth = require('../src/auth');



//CReate product

module.exports.createProduct = (req,res) =>{
    const userData = auth.decode(req.headers.authorization)
    let input = req.body;
    if(!userData.isAdmin){
        res.send(false)
    } else {
        const newProduct = new Product({
            name:input.name,
            description:input.description,
            img:input.img,
            size:input.size,
            color:input.color,
            price:input.price,
            category: input.category
        })
        newProduct.save()
        .then(result =>{
            res.send(result)
        })
        .catch(error => {
            res.send(false)
        })
    }
}

//getAll product


module.exports.getAll = async (req,res) =>{
    
    let input = req.body
     try{   
        const qeuryNew = req.query.new;
        const querycategory = req.query.category;
         let product;
        
        if(qeuryNew){
            product = await Product.find({isActive:true}).sort({createdAt: -1}).limit(5)
        } else if(querycategory){
            product = await Product.find({isActive:true, category:{
                $in:[querycategory],
            }})
        }else{
           product = await Product.find({isActive:true})
        }
          return res.send(product)
        } catch(error){
          return res.send(false)
        }
}

//get all false product

module.exports.getAllFalse = async (req,res) =>{
    
    let input = req.body
     try{   
        const qeuryNew = req.query.new;
        const querycategory = req.query.category;
         let product;
        
        if(qeuryNew){
            product = await Product.find({isActive:false}).sort({createdAt: -1}).limit(5)
        } else if(querycategory){
            product = await Product.find({isActive:false, category:{
                $in:[querycategory],
            }})
        }else{
           product = await Product.find({isActive:false})
        }
          return res.send(product)
        } catch(error){
          return res.send(false)
        }
}

module.exports.archiveProduct = (req,res) =>{
    let id = req.params.id;
    let input = req.body;
    const userData = auth.decode(req.headers.authorization)

    if(!userData.isAdmin){
        res.send(false)
    } else{
        let archived = {
            isActive:input.isActive
        }
        Product.findByIdAndUpdate(id, {$set: archived}, {new: true})
        .then(result => {
            return res.send(result)
        })
        .catch(error => {
            return res.send(false)
        })
    }
}

//Retrieve single product
module.exports.getProduct = (req,res) => {
    let productId = req.params.productId;

    Product.findById(productId)
    .then(result => {
        return res.send(result)
    })
    .catch(error => {
        return res.send(false)
    })
}


module.exports.updateProd = (req,res) =>{
    const userData = auth.decode(req.headers.authorization)
    const id = req.params.id;
    const input = req.body;

    if(!userData.isAdmin){
        return res.send(false)
    } else {
        Product.findById(id)
        .then(result => {
            if(result === null){
                return res.send(false)
            } else {
                let updatedProd = {
                    name:input.name,
                    description:input.description,
                    img:input.img,
                    size:input.size,
                     color:input.color,
                     price:input.price,
                    category: input.category
                }
                Product.findByIdAndUpdate(id, updatedProd, {new:true})
                .then(result => {
                    return res.send(result)
                })
                .catch(error => {
                    console.log(error)
                    return res.send(false)
                })
            }
        })
        .catch(error => {
            console.log(error)
            return res.send(false)
        })
    }
}
