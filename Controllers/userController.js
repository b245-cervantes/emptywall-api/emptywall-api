const mongoose = require('mongoose');
const Users = require('../Models/userSchema');
const bcrypt = require('bcrypt');
const auth = require('../src/auth');



//This controller will create or register a user on our db/database

module.exports.userRegistration = (req,res) => {
    const input = req.body;
    
    Users.findOne({email:input.email})
    .then(result =>{
        if(result !==null){
            console.log(result)
            return res.send(false)
        } else {
            let newUser = new Users ({
                email:input.email,
                firstName:input.firstName,
                lastName:input.lastName,
                password:bcrypt.hashSync(input.password, 10),
                mobileNo:input.mobileNo,
                gender:input.gender
            })
            newUser.save()
            .then(save =>{
                return res.send(true)
            })
            .catch(error => {
                return res.send(error)
            })
        }
    })
    .catch(error => {
        return res.send(error)
    })
    }
    //User authentication
    module.exports.userAuthentication = (req, res) =>{
    let input = req.body;
    //possible scenarios in loggin in
    //1. email is not yet registered
    //2. email is registered but the password is wrong
    
    Users.findOne({email:input.email})
    .then(result => {
    if(result === null){
        return res.send(false)
    } else {
        //we have to verify if the password is correct
        // The "compareSync" method is used to compare a non encrypted password to the encrypted password.
        //it returns boolean value, if match true value will return otherwise false.
        const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)
    
        if(isPasswordCorrect){
            return res.send({auth:auth.createAccessToken(result)})
        }else {
            return res.send(false)
        }
    } 
    })
    .catch(error => {
    res.send(false);
    })
    }
    
    //LOGIN USERS
module.exports.authUser = (req,res) => {
    let input = req.body;

    Users.findOne({email:input.email})
    .then(result =>{
        if(result === null){
            return res.send(false);
        } else {
           const isPasswordCorrect = bcrypt.compareSync(input.password, result.password);
           if(isPasswordCorrect){
            const {password, ...others} = result._doc;
            return res.send({...others, auth:auth.createAccessToken(result)})
           }else{
            res.send(false)
           }
        }
    })
    .catch(error => {
        return res.send(false)
    })
}

//get all users
module.exports.getAll = async(req,res) => {
    
    const userData = auth.decode(req.headers.authorization)
    if(!userData.isAdmin){
    return res.send(false)
         } 
         else  {
            try{
               
                const query = req.query.new;
                const users = query
                 ? await Users.find().sort({_id: -1}).limit(5)
                 : await Users.find();
                 return res.send(users)
            }catch (error){
               return res.send(false)
            }
        
            }
}