const mongoose = require('mongoose');
const Order = require('../Models/orderSchema');
const Users = require('../Models/userSchema');
const Product = require('../Models/productSchema');
const auth = require('../src/auth')




//Creating Orders 
module.exports.createOrder = async (req,res) => {
        let input = req.body;
        let productId = req.body.productId
    const userData = auth.decode(req.headers.authorization)

    if(userData.isAdmin){
        return res.send(false)
    } else {
         let prodIdCorrect = await Product.findById(productId)
         .then(result => {
            result.price = result.price * input.quantity;
           let order = new Order({
            userId: userData._id,
            products: [{productId: result._id, quantity: input.quantity}],
            subTotal: result.price
           })
           order.save()
           .then(save => {
            return res.send(save)
           })
           .catch(error => {
            return res.send(false)
           })
         })
         .catch(error => {
            console.log(error)
            return res.send(false)
         })
    }
}


//GET ALL ORDER ADMIN ONLY
module.exports.getAllOrder = (req,res) => {
    const userData = auth.decode(req.headers.authorization)
    if(!userData.isAdmin){
        res.send("You are not allowed to do that!")
    } else {
        Order.find()
        .then(result => {
            return res.send(result)
        })
        .catch(error =>{
            return res.send(false)
        })
    }
}

//GET AUTH USER ORDER NON-ADMIN ONLY

module.exports.getUserOrder = (req,res) => {
    const userData = auth.decode(req.headers.authorization);
    if(userData.isAdmin){
        res.send("PLEASE LOGIN AS A USERS!")
    } else {
        Order.findOne({userId: userData._id})
        .then(result => {
            return res.send(result)
        })
        .catch(error =>{
           return res.send(error)
        })
    }
}
       

