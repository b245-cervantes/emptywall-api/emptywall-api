const express = require('express');
const app = express();
const port = 4005;
const userRoute = require('../Routes/userRoute')
const mongodb = require('./mongodb');
const cors = require('cors')
const productRoute = require('../Routes/productRoute');
const orderRoute = require('../Routes/orderRoute');

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());


app.use('/user', userRoute)
app.use('/product', productRoute);
app.use('/order', orderRoute);

app.listen(port, ()=> {
    console.log(`The Server is running at port ${port}`)
})
