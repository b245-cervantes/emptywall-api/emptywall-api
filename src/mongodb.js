const express = require('express');
const mongoose = require('mongoose');
const mongodb = express.mongodb;

mongoose.set('strictQuery', true);

mongoose.connect("mongodb+srv://admin:admin@batch245-cervantes.ttw2wvw.mongodb.net/Emptywall_API?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

let db = mongoose.connection;

db.on("error", console.error.bind(console, "error"))
db.once("open", ()=>{
    console.log("We are connected to cloud server")
})

module.exports = mongodb;