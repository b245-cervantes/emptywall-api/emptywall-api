const express = require('express');
const mongoose = require('mongoose');
const router = express.Router();
const userController = require('../Controllers/userController');
const auth = require('../src/auth');

//[Routes]
//this is responsible for the registration of the user
router.post('/register', userController.userRegistration);

//route login for authenticate user
router.post('/login',userController.authUser)

//route for retrieve all user
router.get('/', auth.verify, userController.getAll)

module.exports = router;