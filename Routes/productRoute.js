const express = require('express')
const mongoose = require('mongoose')
const Product = require('../Models/productSchema')
const router = express.Router();
const auth = require('../src/auth')
const productController = require('../Controllers/productController')


//route for creating product
router.post('/create', auth.verify, productController.createProduct);
//route for retreive all product

router.get('/retreive',  productController.getAll)

//route for archiving products or delete
router.put('/archived/:id', auth.verify, productController.archiveProduct)

//route for get all false product
router.get('/deleted', auth.verify, productController.getAllFalse)
//route for retreiving single product
router.get('/:productId', productController.getProduct)

//route for updating product
router.put('/update/:id', auth.verify, productController.updateProd)







module.exports = router