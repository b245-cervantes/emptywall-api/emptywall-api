const mongoose = require('mongoose');

const ProductSchema = new mongoose.Schema({
    name: {
        type:String,
        required: true,
        unique: true
    },
    description: {
        type:String,
        required: true,
    },
    img: {
        type:String,
        required: true
    },
    size: {
        type:String
    },
    color: {
        type:String
    },
    price: {
        type:Number,
        required:true
    },
      category: {
        type:Array,

    },
      isActive: {
        type:Boolean,
        default: true
    } 
}, {timestamps: true});

module.exports = mongoose.model("Product", ProductSchema);