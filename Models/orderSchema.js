const mongoose = require('mongoose');


const OrderSchema = new mongoose.Schema({
    userId: {
        type:String,
        required: true
    },
    products : [
        {
            productId: {
                type:String,
            },
            quantity: {
                type:Number,
                default: 1
            }
        }
    ],
   subTotal: {
        type:Number
    },
    status: {
        type:String,
        default: "Pending"
    }   
},{timestamps: true})

module.exports = mongoose.model("Order", OrderSchema);