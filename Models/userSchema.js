const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true
    },
    firstName: {
        type:String,
        required:true
    },
    lastName : {
        type:String,
        required: true
    },
      birthday: {
        type:Date,
      }
    ,
    password: {
        type:String,
        required:true, 
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    mobileNo :{
        type:String,
        required: true, 
    },
        gender: {
            type:String,
            required: true
        },
    
}, {timestamps: true})

module.exports = mongoose.model('Users', UserSchema);